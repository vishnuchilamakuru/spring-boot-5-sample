package com.vishnu.userservice.controller;

import com.vishnu.userservice.domain.User;
import com.vishnu.userservice.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UserController.BASE_PATH)
@Api(value="users", description="Operations pertaining to users in user service")
public class UserController {

    UserService userService;

    public static final String BASE_PATH = "/v1/users";

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(value = "View List of all Users")
    List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get User by Id")
    User getUserById(@PathVariable("id") Long id) {
        return userService.getUserById(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @ApiOperation(value = "Create User")
    List<User> createUser(@RequestBody List<User> users) {
        return userService.createUsers(users);
    }

}
