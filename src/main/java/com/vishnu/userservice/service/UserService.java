package com.vishnu.userservice.service;

import com.vishnu.userservice.domain.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    User getUserById(Long id);

    List<User> createUsers(List<User> users);
}
