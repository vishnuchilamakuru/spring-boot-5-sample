package com.vishnu.userservice.bootstrap;

import com.vishnu.userservice.domain.User;
import com.vishnu.userservice.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private UserRepository userRepository;

    public BootStrapData(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Loading User Data");

        User u1 = new User();
        u1.setFirstName("vishnu");
        u1.setLastName("chi");
        userRepository.save(u1);

        System.out.println("Imported User Data Successfully : " + userRepository.count());
    }
}
