CREATE TABLE users (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) NOT NULL,
    primary key(id)
);